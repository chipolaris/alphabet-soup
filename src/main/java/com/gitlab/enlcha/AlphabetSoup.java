package com.gitlab.enlcha;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * High level strategy:
 *  
 *  - Use a HashMap to index all entries (char value, coordinate) in the
 *  grid for quick lookup of the initial character of the search word
 *
 *  - When search: for each initial character found, search in all directions: 
 *  	North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest
 *  	Stop search if a match is found
 *  
 *  *Note*: the search function only works for words of at least 2 characters
 */
public class AlphabetSoup {
	
	public static void main(String args[]) {
		
		// make sure there is an argument that point to the input file
		if(args.length == 0) {
			System.out.println(String.format("Usage: java %s <file-path>", AlphabetSoup.class.getName()));
			System.exit(1);
		}
		
		// read input 
		try {
			readInputFile(args[0]);	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		AlphabetSoup alphabetSoup = new AlphabetSoup(inputGrid.length, 
				inputGrid[0].length, inputGrid);
		
		// perform search on each word from the input, store in result list
		List<SearchResult> searchResults = new ArrayList<SearchResult>();
		for(String word : searchWords) {
			searchResults.add(alphabetSoup.search(word));
		}
		
		// output the result
		outputResults(searchResults);
	}
	
	// static members for inputs
	static char[][] inputGrid;
	static List<String> searchWords = new ArrayList<String>();
	
	// instance members
	private GridEntry[][] grid;
	private int rows, cols;
	
	// index of characters and their coordinates, this is for quick lookup of the starting character
	private Map<Character, List<Coordinate>> indexes = new HashMap<Character, List<Coordinate>>();
	
	/**
	 * Read input file
	 * Per spec, it's assumed that the input is properly formatted. 
	 * So no need to check for invalid input
	 * @param filePath
	 */
	private static void readInputFile(String filePath) throws Exception {

		Scanner scanner = new Scanner(new File(filePath));
		
		// read & parse the first line
		String firstLine = scanner.nextLine();
		String[] tokens = firstLine.split("x");
		int rows = Integer.parseInt(tokens[0]);
		int cols = Integer.parseInt(tokens[1]);
		
		// read & parse the grid/matrix
		inputGrid = new char[rows][];
		for(int i = 0; i < rows; i++) {
			inputGrid[i] = new char[cols];
			
			String line = scanner.nextLine();
			
			tokens = line.split("\\s+");
			
			for(int j = 0; j < tokens.length; j++) {
				inputGrid[i][j] = tokens[j].charAt(0); // assume each token is a single char
			}
		}
		
		// read all the search words
		while(scanner.hasNextLine()) {
			searchWords.add(scanner.nextLine().trim());
		}
		
		// cleanup
		scanner.close();
	}

	/**
	 * utility method to output the search result
	 */
	private static void outputResults(List<SearchResult> searchResults) {
		for(SearchResult searchResult : searchResults) {
			System.out.println(String.format("%s %d:%d %d:%d", searchResult.word, 
					searchResult.start.y, searchResult.start.x, 
					searchResult.end.y, searchResult.end.x));
		}
	}
	
	AlphabetSoup(int rows, int cols, char[][] charMatrix) {
		
		this.rows = rows;
		this.cols = cols;
		
		this.grid = new GridEntry[rows][];
		
		for(int i = 0; i < rows; i++) {
			
			grid[i] = new GridEntry[cols];
			for(int j = 0; j < cols; j++) {
				Coordinate coordinate = new Coordinate(i, j);
				grid[i][j] = new GridEntry(coordinate, charMatrix[i][j]);
				
				addToIndexes(charMatrix[i][j], coordinate);
			}
		}
	}
		
	/**
	 * add to indexes, store characters and their positions for quick lookup
	 * @param c
	 * @param coordinate
	 */
	private void addToIndexes(char c, Coordinate coordinate) {
		
		List<Coordinate> coordinates = indexes.get(c);
		if(coordinates == null) {
			coordinates = new ArrayList<AlphabetSoup.Coordinate>();
			indexes.put(c, coordinates);
		}
		
		coordinates.add(coordinate);
	}

	/**
	 * Main search method the grid for the param word, only work for word of at least 2 character long
	 * @param word
	 * @return @SearchR
	 */
	public SearchResult search(String word) {
		
		SearchResult searchResult = new SearchResult(word);
		
		// per spec: 
		// "Words that have spaces in them will not include spaces when hidden in the grid of characters."
		// so, remove all space characters
		word = word.replaceAll("\\s", ""); 
		
		List<Coordinate> coordinates = indexes.get(word.charAt(0));
		
		if(coordinates != null) {
			Coordinate endCoordinate = null;
			// for each coordinate that matches the first character, stop if a match is found		
			for(int i = 0; i < coordinates.size() && endCoordinate == null; i++) {
				
				Coordinate startCoordinate = coordinates.get(i);
				searchResult.start = startCoordinate;
				
				// search each direction (North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest) 
				// stop if a match is found
				for(Direction direction : Direction.values()) {
					endCoordinate = search(word, startCoordinate, 
							new GridIterator(grid[startCoordinate.y][startCoordinate.x], direction));
				
					if(endCoordinate != null) {
						searchResult.end = endCoordinate;
						break;
					}
				}
			}
		}
		
		return searchResult;
	}
	
	/**
	 * Utility method, used by the {@link #search(String)} method above
	 * This traverses the grid starting from the param startCoordinate
	 * go in the direction abstracted by the param iterator
	 * until it can conclude the path is n
	 * @param word
	 * @param startCoordinate
	 * @param iterator
	 * @return
	 */
	private Coordinate search(String word, Coordinate startCoordinate, GridIterator iterator) {
		
		int i = 1;
		
		while(iterator.hasNext() && i < word.length()) {
			
			GridEntry gridEntry = iterator.next();
			
			if(word.charAt(i) != gridEntry.ch) { // not a match, stop the iteration
				break;
			}
			else if(i == word.length() - 1) { // a match of last character of the word, FOUND!!!
				return gridEntry.coordinate;
			}
			// The current char matches but not the end of the search word, so continue
			i++;
		}
		
		return null;
	}
	
	enum Direction {
		NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST
	}
	
	/**
	 * A simple iterator to abstract/simplify the nuisance of 
	 * 	iterating the grid in directions of NORTH, SOUTHEAST, WEST, etc
	 */
	private class GridIterator implements Iterator<GridEntry> {

		Direction direction;
		GridEntry current;
		
		GridIterator(GridEntry gridEntry, Direction direction) {
			this.current = gridEntry;
			this.direction = direction;
		}
		
		public boolean hasNext() {
			
			switch (this.direction) {
				case NORTH:
					return current.coordinate.y > 0;
				case NORTHEAST:
					return current.coordinate.y > 0 && current.coordinate.x < cols - 1;
				case EAST:
					return current.coordinate.x < cols - 1;
				case SOUTHEAST:
					return current.coordinate.y < rows - 1 && current.coordinate.x < cols - 1;
				case SOUTH:
					return current.coordinate.y < rows - 1;
				case SOUTHWEST:
					return current.coordinate.y < rows - 1 && current.coordinate.x > 0;
				case WEST:
					return current.coordinate.x > 0;
				case NORTHWEST:
					return current.coordinate.y > 0 && current.coordinate.x > 0;
				default:
					return false;
			}
		}

		public GridEntry next() {
			
			switch (this.direction) {
				case NORTH:
					this.current = grid[current.coordinate.y - 1][current.coordinate.x];
					break;
				case NORTHEAST:
					this.current = grid[current.coordinate.y - 1][current.coordinate.x + 1];
					break;
				case EAST:
					this.current = grid[current.coordinate.y][current.coordinate.x + 1];
					break;
				case SOUTHEAST:
					this.current = grid[current.coordinate.y + 1][current.coordinate.x + 1];
					break;
				case SOUTH:
					this.current = grid[current.coordinate.y + 1][current.coordinate.x];
					break;
				case SOUTHWEST:
					this.current = grid[current.coordinate.y + 1][current.coordinate.x - 1];
					break;
				case WEST:
					this.current = grid[current.coordinate.y][current.coordinate.x - 1];
					break;
				case NORTHWEST:
					this.current = grid[current.coordinate.y - 1][current.coordinate.x - 1];
					break;
				default:
					this.current = null;
			}
			
			return this.current;
		}
	}

	class Coordinate {
		int y, x;
		
		Coordinate(int y, int x) {
			this.x = x;
			this.y = y;
		}
	}
	
	/**
	 * Store character value and coordinate x and y
	 * for an entry in the grid
	 */
	class GridEntry {
		Coordinate coordinate;
		char ch;
		
		GridEntry(Coordinate coordinate, char ch) {
			this.coordinate = coordinate;
			this.ch = ch;
		}
	}
	
	/**
	 * Store search result 
	 */
	class SearchResult {
		
		// the word to search
		String word;
		
		// the coordinates of the result, 
		// if not found, the 'end' coordinate would be (-1, -1)
		Coordinate start, end;
		
		SearchResult(String word) {
			this.word = word;
			
			// initialized coordinates to -1;
			this.start = new Coordinate(-1, -1);
			this.end = new Coordinate(-1, -1);
		}
	}
}
